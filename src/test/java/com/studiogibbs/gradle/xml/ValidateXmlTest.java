package com.studiogibbs.gradle.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.gradle.testkit.runner.TaskOutcome;
import org.junit.jupiter.api.Test;

public class ValidateXmlTest {
  
  @Test
  public void projectFile() throws URISyntaxException {
    URL testBuild = ValidateXmlTest.class.getResource("/build.gradle");
    File testBuildFile = new File(testBuild.toURI());
    
    BuildResult result = GradleRunner.create()
        .withDebug(true)
        .withProjectDir(testBuildFile.getParentFile())
        .withArguments("validateXml")
        .buildAndFail();
    
    assertEquals(TaskOutcome.FAILED, result.task(":validateXml").getOutcome());
    
    String output = result.getOutput();
    
    assertTrue(output.contains("- ERROR: cvc-complex-type.4: Attribute 'orderid' must appear on element 'shiporder'."));
  }
  
  @Test
  public void validationPasses() throws URISyntaxException {
    URL testBuild = ValidateXmlTest.class.getResource("/build.gradle");
    File testBuildFile = new File(testBuild.toURI());
    
    BuildResult result = GradleRunner.create()
        .withDebug(true)
        .withProjectDir(testBuildFile.getParentFile())
        .withArguments("validateValid")
        .build();
    
    assertEquals(TaskOutcome.SUCCESS, result.task(":validateValid").getOutcome());
    
    String output = result.getOutput();
    
    assertTrue(output.contains("valid.xml"));
  }
  
  @Test
  public void validationFils() throws URISyntaxException {
    URL testBuild = ValidateXmlTest.class.getResource("/build.gradle");
    File testBuildFile = new File(testBuild.toURI());
    
    BuildResult result = GradleRunner.create()
        .withDebug(true)
        .withProjectDir(testBuildFile.getParentFile())
        .withArguments("validateInvalid")
        .buildAndFail();
    
    assertEquals(TaskOutcome.FAILED, result.task(":validateInvalid").getOutcome());
    
    String output = result.getOutput();
    
    assertTrue(output.contains("- ERROR: cvc-complex-type.4: Attribute 'orderid' must appear on element 'shiporder'."));
  }

  @Test
  public void canAddTaskToProject() {
    Project project = ProjectBuilder.builder().build();
    project.getPluginManager().apply("com.studiogibbs.gradle.xml-validator");
    
    assertTrue(project.getPluginManager().hasPlugin("com.studiogibbs.gradle.xml-validator"));
    
    Task task = project.getTasks().getByName("validateXml");
    assertNotNull(task);
    
    assertEquals("verification", task.getGroup());
  }
  
  @Test
  public void validPasses() throws URISyntaxException {
    Project project = ProjectBuilder.builder().build();
    project.getPluginManager().apply(XmlValidatorPlugin.class);
    
    URL schema = ValidateXmlTest.class.getResource("/simple.xsd");
    File xsd = new File(schema.toURI());
    
    URL valid = ValidateXmlTest.class.getResource("/testfiles/valid.xml");
    File source = new File(valid.toURI());
    
    ValidateXml xvt = project.getTasks().create("test-validate", ValidateXml.class, t -> {
      t.getSchemaFile().set(xsd);
      t.setSourceFiles(project.fileTree(source));
    });
    
    xvt.validate();
  }
  

  @Test
  public void invalidFails() throws URISyntaxException {
    Project project = ProjectBuilder.builder().build();
    project.getPluginManager().apply(XmlValidatorPlugin.class);
    
    URL schema = ValidateXmlTest.class.getResource("/simple.xsd");
    File xsd = new File(schema.toURI());
    
    URL valid = ValidateXmlTest.class.getResource("/testfiles/invalid.xml");
    File source = new File(valid.toURI());
    
    ValidateXml xvt = project.getTasks().create("test-validate", ValidateXml.class, t -> {
      t.getSchemaFile().set(xsd);
      t.setSourceFiles(project.fileTree(source));
    });
    
    assertThrows(GradleException.class, xvt::validate);
  }
}
