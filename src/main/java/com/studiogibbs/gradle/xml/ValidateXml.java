package com.studiogibbs.gradle.xml;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.xml.XMLConstants;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.file.ConfigurableFileTree;
import org.gradle.api.file.Directory;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.logging.Logger;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.PathSensitive;
import org.gradle.api.tasks.PathSensitivity;
import org.gradle.api.tasks.TaskAction;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ValidateXml extends DefaultTask {

  private final class SimpleErrorHandler implements ErrorHandler {
    
    private List<SAXParseException> warnings = new LinkedList<>();
    private List<SAXParseException> errors = new LinkedList<>();
    private List<SAXParseException> fatalErrors = new LinkedList<>();
    
    private final Logger logger;
    
    public SimpleErrorHandler(Logger logger) {
      this.logger = logger;
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
      logger.warn("- WARNING: {}", exception.getMessage());
      warnings.add(exception);
    }
    
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
      logger.error("- FATAL ERROR: {}", exception.getMessage());
      fatalErrors.add(exception);
    }
    
    @Override
    public void error(SAXParseException exception) throws SAXException {
      logger.error("- ERROR: {}", exception.getMessage());
      errors.add(exception);
    }
    
    public void clear() {
      warnings.clear();
      errors.clear();
      fatalErrors.clear();
    }

    public void haltOnError() throws GradleException {
      
      if(warnings.isEmpty() && errors.isEmpty() && fatalErrors.isEmpty()) {
        return;
      }
      
      GradleException ex = new GradleException("Errors Validating XML");
      
      warnings.forEach(ex::addSuppressed);
      errors.forEach(ex::addSuppressed);
      fatalErrors.forEach(ex::addSuppressed);
      
      throw ex;
    }
  }
  
  private final SchemaFactory schemaFactory;
  private final SimpleErrorHandler errorHandler;

  private ConfigurableFileTree sourceFiles;
  private final RegularFileProperty schemaFile;
  private final DirectoryProperty reportDir;

  private Validator validator;
  
  @Inject
  public ValidateXml(ObjectFactory objects) {
//    sourceFiles = objects.fileTree();
    schemaFile = objects.fileProperty();
    reportDir = objects.directoryProperty();
    
    schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    
    errorHandler = new SimpleErrorHandler(getLogger());
    
    setGroup("verification");
  }

  @InputFile
  public RegularFileProperty getSchemaFile() {
    return schemaFile;
  }

  @InputFiles
  @PathSensitive(PathSensitivity.NAME_ONLY)
  public ConfigurableFileTree getSourceFiles() {
    return sourceFiles;
  }
  
  public void setSourceFiles(ConfigurableFileTree sourceFiles) {
    this.sourceFiles = sourceFiles;
  }
  
  @OutputDirectory
  @Optional
  public DirectoryProperty getReportDir() {
    return reportDir;
  }
  

  @TaskAction
  public void validate() {
    Result result = null;
    
    errorHandler.clear();
    
    try {
      Schema xsd = schemaFactory.newSchema(getSchemaFile().get().getAsFile());
      validator = xsd.newValidator();
      
      validator.setErrorHandler(errorHandler);
      
    } catch (SAXException e) {
      throw new RuntimeException(e);
    }
    
    File reportDir = getReportDir().map(Directory::getAsFile).getOrNull();
    
    getLogger().quiet("[VALIDATING]");
    
    Set<File> files = getSourceFiles().getFiles();
    for (File src : files) {
      
      if (reportDir != null) {
        String reportName = src.getName().replace('.', '-') + "-report.xml";
        result = new StreamResult(new File(reportDir, reportName));
      }
      
      getLogger().quiet("{}", src.getPath());
      
      try {
        validator.validate(new StreamSource(src), result);
      } catch (SAXException | IOException e) {
        throw new GradleException("Error running validation", e);
      }
    }
    
    errorHandler.haltOnError();
  }
}
