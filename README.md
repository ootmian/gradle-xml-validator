Gradle XML Validator
====================

My first attempt at a custom gradle plugin, written in Java.

Provides a single task to validate XML files according to a provided schema.
