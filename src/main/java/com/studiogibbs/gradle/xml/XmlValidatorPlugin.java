package com.studiogibbs.gradle.xml;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.ConfigurableFileTree;

public class XmlValidatorPlugin implements Plugin<Project> {

  @Override
  public void apply(Project project) {
    project.getTasks().create("validateXml", ValidateXml.class, t -> {
      ConfigurableFileTree defSrc = project.fileTree("src/main/resources", ft -> ft.include("**/*.xml"));
      t.setSourceFiles(defSrc);
    });
  }

}
